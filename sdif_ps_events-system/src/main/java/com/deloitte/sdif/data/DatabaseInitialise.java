package com.deloitte.sdif.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.springframework.beans.factory.InitializingBean;

public class DatabaseInitialise implements InitializingBean {

	public void afterPropertiesSet() throws Exception {

		String dbURL = "jdbc:derby:memory:local;create=true";
		Connection conn = null;

		String AlarmReplayNewSampleFile = "ALARM_REPLAY_NEW.csv";
		String AlarmReplayArchiveSampleFile = "ALARM_REPLAY_ARCHIVE.csv";

		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
			conn = DriverManager.getConnection(dbURL);
			Statement stmt = conn.createStatement();
			
			// create ALARM_REPLAY_NEW table
			stmt.executeUpdate("CREATE TABLE ALARM_REPLAY_NEW ("
					+ "ID VARCHAR(255)," 
					+ "ALARM_ID VARCHAR(255)," 
					+ "ALARM_TIME VARCHAR(255)," 
					+ "ALARM_USECS VARCHAR(255)," 
					+ "ALARM_INITIAL_TIME VARCHAR(255)," 
					+ "ALARM_INITIAL_USECS VARCHAR(255),"
					+ "ALARM_PRIORITY VARCHAR(255),"
					+ "ALARM_TYPE VARCHAR(255),"
					+ "ALARM_TEXT VARCHAR(255),"
					+ "ALARM_COMPONENT_ALIAS VARCHAR(255),"
					+ "ALARM_DISTRICT_ZONE VARCHAR(255),"
					+ "ALARM_SUBSTATION_ALIAS VARCHAR(255),"
					+ "ALARM_SUBSTATION_NAME VARCHAR(255),"
					+ "ALARM_ACK_TIME VARCHAR(255),"
					+ "ALARM_ACK_USECS VARCHAR(255),"
					+ "ALARM_NAME VARCHAR(255),"
					+ "ALARM_BUSBAR_NUM VARCHAR(255),"
					+ "ALARM_CIRCUIT_REF VARCHAR(255),"
					+ "ALARM_CIRCUIT_NAME VARCHAR(255),"
					+ "DEVICE_TYPE VARCHAR(255),"
					+ "AREA VARCHAR(255),"
					+ "OPERATOR_ACTION VARCHAR(255),"
					+ "DATASOURCEID VARCHAR(255),"
					+ "LOCALDATETIME VARCHAR(255),"
					+ "SUPPLEMENTARY VARCHAR(255),"
					+ "COMPONENT_PATHNAME VARCHAR(255),"
					+ "ALARM_TEXT2 VARCHAR(255),"
					+ "DESCRIPTOR VARCHAR(255),"
					+ "PRIMARY_BUSBAR VARCHAR(255),"
					+ "PRIMARY_FEEDER VARCHAR(255))"); 
			
			// write sample file into ALARM_REPLAY_NEW table
			br = new BufferedReader(new InputStreamReader(getClass()
					.getClassLoader().getResourceAsStream(AlarmReplayNewSampleFile)));
			while ((line = br.readLine()) != null) {
				String[] clientsLineEntry = line.split(csvSplitBy);			
				
				stmt.executeUpdate("INSERT INTO ALARM_REPLAY_NEW ("
						+ "ID," 
						+ "ALARM_ID," 
						+ "ALARM_TIME," 
						+ "ALARM_USECS," 
						+ "ALARM_INITIAL_TIME," 
						+ "ALARM_INITIAL_USECS,"
						+ "ALARM_PRIORITY,"
						+ "ALARM_TYPE,"
						+ "ALARM_TEXT,"
						+ "ALARM_COMPONENT_ALIAS,"
						+ "ALARM_DISTRICT_ZONE,"
						+ "ALARM_SUBSTATION_ALIAS,"
						+ "ALARM_SUBSTATION_NAME,"
						+ "ALARM_ACK_TIME,"
						+ "ALARM_ACK_USECS,"
						+ "ALARM_NAME,"
						+ "ALARM_BUSBAR_NUM,"
						+ "ALARM_CIRCUIT_REF,"
						+ "ALARM_CIRCUIT_NAME,"
						+ "DEVICE_TYPE,"
						+ "AREA,"
						+ "OPERATOR_ACTION,"
						+ "DATASOURCEID,"
						+ "LOCALDATETIME,"
						+ "SUPPLEMENTARY,"
						+ "COMPONENT_PATHNAME,"
						+ "ALARM_TEXT2,"
						+ "DESCRIPTOR,"
						+ "PRIMARY_BUSBAR,"
						+ "PRIMARY_FEEDER) " 
						+ "VALUES ('" 
						+ clientsLineEntry[0] + "','" 
						+ clientsLineEntry[1] + "','" 
						+ clientsLineEntry[2] + "','" 
						+ clientsLineEntry[3] + "','" 
						+ clientsLineEntry[4] + "','" 
						+ clientsLineEntry[5] + "','" 
						+ clientsLineEntry[6] + "','" 
						+ clientsLineEntry[7] + "','" 
						+ clientsLineEntry[8] + "','"
						+ clientsLineEntry[9] + "','"
						+ clientsLineEntry[10] + "','"
						+ clientsLineEntry[11] + "','"
						+ clientsLineEntry[12] + "','"
						+ clientsLineEntry[13] + "','"
						+ clientsLineEntry[14] + "','"
						+ clientsLineEntry[15] + "','"
						+ clientsLineEntry[16] + "','"
						+ clientsLineEntry[17] + "','"
						+ clientsLineEntry[18] + "','"
						+ clientsLineEntry[19] + "','"
						+ clientsLineEntry[20] + "','"
						+ clientsLineEntry[21] + "','"
						+ clientsLineEntry[22] + "','"
						+ clientsLineEntry[23] + "','"
						+ clientsLineEntry[24] + "','"
						+ clientsLineEntry[25] + "','"
						+ clientsLineEntry[26] + "','"
						+ clientsLineEntry[27] + "','"
						+ clientsLineEntry[28] + "','"
						+ clientsLineEntry[29] + "')");
			}
				
			// create ALARM_REPLAY_ARCHIVE table
			stmt.executeUpdate("CREATE TABLE ALARM_REPLAY_ARCHIVE ("
					+ "ALARM_ID VARCHAR(255)," 
					+ "ALARM_TIME VARCHAR(255)," 
					+ "ALARM_USECS VARCHAR(255)," 
					+ "ALARM_INITIAL_TIME VARCHAR(255)," 
					+ "ALARM_INITIAL_USECS VARCHAR(255),"
					+ "ALARM_PRIORITY VARCHAR(255),"
					+ "ALARM_TYPE VARCHAR(255),"
					+ "ALARM_TEXT VARCHAR(255),"
					+ "ALARM_COMPONENT_ALIAS VARCHAR(255),"
					+ "ALARM_DISTRICT_ZONE VARCHAR(255),"
					+ "ALARM_SUBSTATION_ALIAS VARCHAR(255),"
					+ "ALARM_SUBSTATION_NAME VARCHAR(255),"
					+ "ALARM_ACK_TIME VARCHAR(255),"
					+ "ALARM_ACK_USECS VARCHAR(255),"
					+ "ALARM_NAME VARCHAR(255),"
					+ "ALARM_BUSBAR_NUM VARCHAR(255),"
					+ "ALARM_CIRCUIT_REF VARCHAR(255),"
					+ "ALARM_CIRCUIT_NAME VARCHAR(255),"
					+ "DEVICE_TYPE VARCHAR(255),"
					+ "AREA VARCHAR(255),"
					+ "OPERATOR_ACTION VARCHAR(255),"
					+ "DATASOURCEID VARCHAR(255),"
					+ "LOCALDATETIME VARCHAR(255),"
					+ "SUPPLEMENTARY VARCHAR(255),"
					+ "COMPONENT_PATHNAME VARCHAR(255),"
					+ "ALARM_TEXT2 VARCHAR(255),"
					+ "DESCRIPTOR VARCHAR(255),"
					+ "PRIMARY_BUSBAR VARCHAR(255),"
					+ "PRIMARY_FEEDER VARCHAR(255))"); 
			
			// write sample file into ALARM_REPLAY_NEW table
			br = new BufferedReader(new InputStreamReader(getClass()
					.getClassLoader().getResourceAsStream(AlarmReplayArchiveSampleFile)));
			while ((line = br.readLine()) != null) {
				String[] institutionsLineEntry = line.split(csvSplitBy);			
				
				stmt.executeUpdate("INSERT INTO ALARM_REPLAY_ARCHIVE ("
					+ "ALARM_ID," 
					+ "ALARM_TIME," 
					+ "ALARM_USECS," 
					+ "ALARM_INITIAL_TIME," 
					+ "ALARM_INITIAL_USECS,"
					+ "ALARM_PRIORITY,"
					+ "ALARM_TYPE,"
					+ "ALARM_TEXT,"
					+ "ALARM_COMPONENT_ALIAS,"
					+ "ALARM_DISTRICT_ZONE,"
					+ "ALARM_SUBSTATION_ALIAS,"
					+ "ALARM_SUBSTATION_NAME,"
					+ "ALARM_ACK_TIME,"
					+ "ALARM_ACK_USECS,"
					+ "ALARM_NAME,"
					+ "ALARM_BUSBAR_NUM,"
					+ "ALARM_CIRCUIT_REF,"
					+ "ALARM_CIRCUIT_NAME,"
					+ "DEVICE_TYPE,"
					+ "AREA,"
					+ "OPERATOR_ACTION,"
					+ "DATASOURCEID,"
					+ "LOCALDATETIME,"
					+ "SUPPLEMENTARY,"
					+ "COMPONENT_PATHNAME,"
					+ "ALARM_TEXT2,"
					+ "DESCRIPTOR,"
					+ "PRIMARY_BUSBAR,"
					+ "PRIMARY_FEEDER) " 
					+ "VALUES ('" 
					+ institutionsLineEntry[0] + "','" 
					+ institutionsLineEntry[1] + "','" 
					+ institutionsLineEntry[2] + "','" 
					+ institutionsLineEntry[3] + "','" 
					+ institutionsLineEntry[4] + "','" 
					+ institutionsLineEntry[5] + "','" 
					+ institutionsLineEntry[6] + "','" 
					+ institutionsLineEntry[7] + "','" 
					+ institutionsLineEntry[8] + "','"
					+ institutionsLineEntry[9] + "','"
					+ institutionsLineEntry[10] + "','"
					+ institutionsLineEntry[11] + "','"
					+ institutionsLineEntry[12] + "','"
					+ institutionsLineEntry[13] + "','"
					+ institutionsLineEntry[14] + "','"
					+ institutionsLineEntry[15] + "','"
					+ institutionsLineEntry[16] + "','"
					+ institutionsLineEntry[17] + "','"
					+ institutionsLineEntry[18] + "','"
					+ institutionsLineEntry[19] + "','"
					+ institutionsLineEntry[20] + "','"
					+ institutionsLineEntry[21] + "','"
					+ institutionsLineEntry[22] + "','"
					+ institutionsLineEntry[23] + "','"
					+ institutionsLineEntry[24] + "','"
					+ institutionsLineEntry[25] + "','"
					+ institutionsLineEntry[26] + "','"
					+ institutionsLineEntry[27] + "','"
					+ institutionsLineEntry[28] + "')");
			}
		} catch (java.sql.SQLException sqle) {
			sqle.printStackTrace();
			throw sqle;
		}
	}
}